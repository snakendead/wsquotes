//
//  QuotesCollectionHelper.swift
//  WSQuotes
//
//  Created by Vladimir Vasilyev on 31.07.2017.
//

import UIKit

class QuotesCollectionHelper: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var dataCount: Int = 0
    
    var dataFillHandler:((_ indexPath: Int, _ cell: UITableViewCell) -> ())?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: nil)
        dataFillHandler?(indexPath.row, cell)
        return cell
    }
}

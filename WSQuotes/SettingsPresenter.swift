//
//  SettingsPresenter.swift
//  WSQuotes
//
//  Created by Vladimir Vasilyev on 31.07.2017.
//

import UIKit

class SettingsPresenter: NSObject {
    weak var view: SettingsViewController!
    weak var collectionHelper: SettingsCollectionHelper! {
        didSet {
            setupCollectionHelper()
        }
    }
    
    var interactor = SettingsInteractor()
    
    private var savedInstruments = [Instrument]()
    
    private func setupCollectionHelper() {
        collectionHelper.dataFillHandler = {
            [weak self]
            (indexPath, cell) in
            guard let self_ = self else { return }
            let instrument = self_.savedInstruments[indexPath]
            let text = "\(instrument.symbol.rawValue)  enabled - \(instrument.enabled)"
            cell.textLabel?.text = text
        }
        
        collectionHelper.cellTapAction = {
            [weak self]
            (indexPath) in
            guard let self_ = self else { return }
            let instrument = self_.savedInstruments[indexPath]
            instrument.enabled = !instrument.enabled
            self_.interactor.update(instrument: instrument)
            self_.view.reloadTableView()
        }
        
        collectionHelper.dataCount = 0
    }
    
    func viewIsReady() {
        interactor.output = self
    }
    
    func viewAppeared() {
        interactor.getInstruments()
    }
    
    func instrumentsReceived(_ instruments: [Instrument]) {
        savedInstruments = instruments
        collectionHelper.dataCount = instruments.count
        view.reloadTableView()
    }
    
}

//
//  QuotesPresenter.swift
//  WSQuotes
//
//  Created by Vladimir Vasilyev on 30.07.2017.
//

import UIKit

class QuotesPresenter: NSObject {
    
    weak var view: QuotesViewController!
    weak var collectionHelper: QuotesCollectionHelper! {
        didSet {
            setupCollectionHelper()
        }
    }
    var interactor = QuotesInteractor()
    
    private var savedInstruments = [Instrument]()
    
    private func setupCollectionHelper() {
        collectionHelper.dataFillHandler = {
            [weak self]
            (indexPath, cell) in
            guard let self_ = self else { return }
            let instrument = self_.savedInstruments[indexPath]
            cell.textLabel?.text = self_.formattedCellText(for: instrument.lastTick)
            cell.textLabel?.textColor = !instrument.enabled ? UIColor.red : UIColor.black
        }
        
        collectionHelper.dataCount = 0
    }
    
    func viewIsReady() {
        interactor.output = self
    }
    
    func viewAppeared() {
        interactor.getInstruments()
    }
    
    func viewDisappear() {
        interactor.stopSubscribing(for: savedInstruments)
        savedInstruments.removeAll()
    }
    
    func instrumentsReceived(_ instruments: [Instrument]) {
        savedInstruments = instruments
        collectionHelper.dataCount = instruments.count
        interactor.subscribeTicks(for: instruments)
    }
    
    func tickReceived(for instrument: Instrument) {
        guard let index = self.savedInstruments.index(of: instrument) else { return }
        view.reloadCells(for: [index])
    }
    
    // This method must be inside vustom cell View class, but it's simplified now
    private func formattedCellText(for tick: Tick?) -> String {
        guard let safeTick = tick else { return "–" }
        let text = "\(safeTick.symbol) || \(safeTick.bid) / \(safeTick.ask) || \(safeTick.spread)"
        return text
    }
}

//
//  QuotesInteractor.swift
//  WSQuotes
//
//  Created by Vladimir Vasilyev on 30.07.2017.
//

import UIKit

class QuotesInteractor: NSObject {
    
    weak var output: QuotesPresenter!
    
    private var symbolInstruments = [String: Instrument]()
    
    func subscribeTicks(for instruments: [Instrument]) {
        for instrument in instruments {
            symbolInstruments[instrument.symbol.rawValue] = instrument
            self.output.tickReceived(for: instrument)
            
            if (instrument.enabled) {
                QuotePublisherService.shared.enableSymbol(instrument.symbol)
            }
        }
        
        QuotePublisherService.shared.subscribeOnTicks {
            (tickPair) in
            for (key, value) in tickPair {
                guard let instrument = self.symbolInstruments[key.rawValue] else { continue }
                instrument.lastTick = value
                SimpleDatabase.shared.save(instrument: instrument)
                self.output.tickReceived(for: instrument)
            }
        }
    }
    
    func stopSubscribing(for instruments: [Instrument]) {
        for instrument in instruments {
            symbolInstruments[instrument.symbol.rawValue] = instrument
            QuotePublisherService.shared.disableSymbol(instrument.symbol)
        }
    }
    
    func getInstruments() {
        var instruments = [Instrument]()
        
        defer {
            output.instrumentsReceived(instruments)
        }
        
        let restored = SimpleDatabase.shared.restoreInstruments()
            
        if restored.count != 0 {
            instruments = restored
        } else {
            instruments.append(Instrument(with: SymbolPair.eurusd))
            instruments.append(Instrument(with: SymbolPair.gbpusd))
            SimpleDatabase.shared.save(instruments: instruments)
        }
        
    }
    
}

//
//  SettingsInteractor.swift
//  WSQuotes
//
//  Created by Vladimir Vasilyev on 30.07.2017.
//

import UIKit

class SettingsInteractor: NSObject {
    
    weak var output: SettingsPresenter!
    
    func getInstruments() {
        let restored = SimpleDatabase.shared.restoreInstruments()
        output.instrumentsReceived(restored)
    }
    
    func update(instrument: Instrument) {
        SimpleDatabase.shared.save(instrument: instrument)
    }
    
    func remove(instrument: Instrument) {
        SimpleDatabase.shared.removeInstrument(instrument: instrument)
    }
}

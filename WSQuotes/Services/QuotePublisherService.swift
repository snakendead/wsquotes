//
//  QuotePublisherService.swift
//  WSQuotes
//
//  Created by Vladimir Vasilyev on 30.07.2017.
//

import UIKit

enum SymbolPair: String {
    case eurusd = "EURUSD"
    case eurgbp = "EURGBP"
    case usdjpy = "USDJPY"
    case gbpusd = "GBPUSD"
    case usdchf = "USDCHF"
    case usdcad = "USDCAD"
    case audusd = "AUDUSD"
    case eurjpy = "EURJPY"
    case eurchf = "EURCHF"
}

class QuotePublisherService: NSObject {
    
    static let shared = QuotePublisherService()
    
    private let ws = WebSocket("wss://quotes.exness.com:18400")
    private var subscriberClosure: (([SymbolPair: Tick]) -> ())?
    
    private var openedSymbols = [SymbolPair]()
    
    //MARK: Public
    
    override init() {
        super.init()
        setupService()
    }
    
    func enableSymbol(_ symbol: SymbolPair) {
        addSymbol(symbol) {
            self.ws.send(text: "SUBSCRIBE: " + symbol.rawValue)
        }
    }
    
    func disableSymbol(_ symbol: SymbolPair) {
        removeSymbol(symbol) {
            self.ws.send(text: "UNSUBSCRIBE: " + symbol.rawValue)
        }
    }
    
    func subscribeOnTicks(_ closure: @escaping ([SymbolPair: Tick]) -> ()) {
        subscriberClosure = closure
    }
    
    private func setupService() {
        ws.event.message = { message in
            guard let text = message as? String else {
                return
            }
            self.parseSocketMessage(text: text)
        }
    }
    
    private func addSymbol(_ symbol: SymbolPair, callback: @escaping ()->()) {
        
        openedSymbols.append(symbol)
        guard openedSymbols.count == 1 else {
            callback()
            return
        }
        
        ws.event.open = {
            callback()
        }
    }
    
    private func removeSymbol(_ symbol: SymbolPair, callback: @escaping ()->()) {
        guard let index = openedSymbols.index(of: symbol) else {
            print("We havn't symbol - \(symbol.rawValue)")
            callback()
            return
        }
        
        openedSymbols.remove(at: index)
        guard openedSymbols.count == 0 else {
            callback()
            return
        }
        
        ws.event.close = { code, reason, clean in
            callback()
        }
    }
    
    private func parseSocketMessage(text: String) {
        let data = text.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        guard let jsonData = data else { return }
        
        let json = try! JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
        let dict = json as! Dictionary <String, Any>
        
        var ticks: [Tick] = []
        
        var ticksJson: [Dictionary <String, Any>]? = nil
        
        if let subscriberList = dict["subscribed_list"] as? Dictionary <String, Any> {
            ticksJson = subscriberList["ticks"] as? [Dictionary<String, Any>]
        } else {
            ticksJson = dict["ticks"] as? [Dictionary<String, Any>]
        }
        
        guard let ticksJsonSafe = ticksJson else { return }
        
        for jsonDict: Dictionary<String, Any> in ticksJsonSafe {
            let tick = Tick(with: jsonDict)
            ticks.append(tick)
        }
        
        sendTicks(ticks)
    }
    
    private func sendTicks(_ ticks: [Tick]) {
        var tickDict: [SymbolPair: Tick] = [:]
        for tick in ticks {
            guard let symbol = SymbolPair.init(rawValue: tick.symbol) else { continue }
            tickDict[symbol] = tick
        }
        
        subscriberClosure?(tickDict)
    }
}

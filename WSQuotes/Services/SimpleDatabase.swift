//
//  SimpleDatabase.swift
//  WSQuotes
//
//  Created by Vladimir Vasilyev on 31.07.2017.
//

import UIKit

class SimpleDatabase: NSObject {
    
    static let shared = SimpleDatabase()
    
    private let defaults = UserDefaults.standard
    
    private let kStoredInstruments = "kStoredInstruments"
    private let kStoredInstrumentsEnabled = "kStoredInstrumentsEnabled"
    private let kStoredInstrumentsTick = "kStoredInstrumentsTick"
    private let kStoredInstrumentsPosition = "kStoredInstrumentsPosition"
    
    func restoreInstruments() -> [Instrument] {
        
        guard let instrumentSymbols = defaults.stringArray(forKey: kStoredInstruments) else {
            return []
        }
        
        var instruments = [Instrument]()
        for symbol in instrumentSymbols {
            guard let symbolPair = SymbolPair(rawValue: symbol) else { continue }
            let instrument = Instrument(with: symbolPair)
            instruments.append(instrument)
        }
        
        fillRestored(instruments: &instruments)
        return instruments
    }
    
    private func fillRestored(instruments: inout [Instrument]) {
        for instrument in instruments {
            guard let lastTick = instrumentRestoredTick(instrument) else { continue }
            instrument.lastTick = lastTick
            
            let position = instrumentRestoredPosition(instrument)
            instrument.position = position
            
            let enabled = instrumentRestoredEnabled(instrument)
            instrument.enabled = enabled
        }
    }
    
    private func instrumentRestoredTick(_ instrument: Instrument) -> Tick? {
        let key = instrumentRestoredKey(instrument, key: kStoredInstrumentsTick)
        guard let value = defaults.dictionary(forKey: key) as? [String: String] else { return nil }
        let lastTick = Tick(with: value)
        return lastTick
    }
    
    private func instrumentRestoredPosition(_ instrument: Instrument) -> Int {
        let key = instrumentRestoredKey(instrument, key: kStoredInstrumentsPosition)
        let value = defaults.integer(forKey: key)
        return value
    }
    
    private func instrumentRestoredEnabled(_ instrument: Instrument) -> Bool {
        let key = instrumentRestoredKey(instrument, key: kStoredInstrumentsEnabled)
        let enabled = defaults.bool(forKey: key)
        return enabled
    }

    private func instrumentRestoredKey(_ instrument: Instrument, key: String) -> String {
        let key = key + "_" + instrument.symbol.rawValue
        return key
    }
    
    func save(instruments: [Instrument]) {
        for instrument in instruments {
            save(instrument: instrument)
        }
    }
    
    func save(instrument: Instrument) {
        var instrumentSymbols = [String]()
        if let instruments = defaults.stringArray(forKey: kStoredInstruments) {
            instrumentSymbols = instruments
        }
        if instrumentSymbols.index(of: instrument.symbol.rawValue) == nil {
            instrumentSymbols.append(instrument.symbol.rawValue)
            defaults.set(instrumentSymbols, forKey: kStoredInstruments)
        }
        
        let keyEnabled = instrumentRestoredKey(instrument, key: kStoredInstrumentsEnabled)
        let keyTick = instrumentRestoredKey(instrument, key: kStoredInstrumentsTick)
        
        defaults.set(instrument.enabled, forKey: keyEnabled)
        defaults.set(instrument.lastTick?.toJsonString(), forKey: keyTick)
    }
    
    func removeInstrument(instrument: Instrument) {
        guard var instrumentSymbols = defaults.stringArray(forKey: kStoredInstruments) else {
            return
        }
        guard let index = instrumentSymbols.index(of: instrument.symbol.rawValue) else {
            return
        }
        instrumentSymbols.remove(at: index)
    }
}

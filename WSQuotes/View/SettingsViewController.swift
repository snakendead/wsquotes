//
//  SettingsViewController.swift
//  WSQuotes
//
//  Created by Vladimir Vasilyev on 30.07.2017.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var output = SettingsPresenter()
    private var collectionHelper = SettingsCollectionHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        output.viewIsReady()
    }
    
    private func setupView() {
        output.view = self
        output.collectionHelper = collectionHelper
        
        tableView.delegate = collectionHelper
        tableView.dataSource = collectionHelper
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output.viewAppeared()
    }
    
    func reloadTableView() {
        self.tableView.reloadData()
    }
}

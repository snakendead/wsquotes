//
//  QuotesViewController.swift
//  WSQuotes
//
//  Created by Vladimir Vasilyev on 30.07.2017.
//

import UIKit

class QuotesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var settingsButton: UIButton!
    
    private var output = QuotesPresenter()
    private var collectionHelper = QuotesCollectionHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        output.viewIsReady()
    }
    
    private func setupView() {
        output.view = self
        output.collectionHelper = collectionHelper
        
        tableView.delegate = collectionHelper
        tableView.dataSource = collectionHelper
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output.viewAppeared()
    }
    
    @IBAction func settingsButtonAction(_ sender: UIButton) {
        performSegue(withIdentifier: "QuotesSettingsSegue", sender: nil)
    }
    
    func reloadCells(for paths: [Int]) {
        // simplified now
        tableView.reloadData()
    }
}

//
//  Tick.swift
//  WSQuotes
//
//  Created by Vladimir Vasilyev on 30.07.2017.
//

import UIKit

class Tick: NSObject {
    
    var symbol: String
    var bid: Float
    var ask: Float
    var spread: Float
    
    init(with jsonDict:[String: Any]) {
        self.symbol = jsonDict["s"] as? String ?? ""
        let bid = jsonDict["b"] as? String ?? ""
        let ask = jsonDict["a"] as? String ?? ""
        let spread = jsonDict["spr"] as? String ?? ""
        
        self.bid = Float(bid) ?? 0.0
        self.ask = Float(ask) ?? 0.0
        self.spread = Float(spread) ?? 0.0
        super.init()
    }
    
    func toJsonString() -> [String: String] {
        var json = [String: String]()
        json["s"] = self.symbol
        json["b"] = "\(self.bid)"
        json["a"] = "\(self.ask)"
        json["spr"] = "\(self.spread)"
        return json
    }
}

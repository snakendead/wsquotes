//
//  Instrument.swift
//  WSQuotes
//
//  Created by Vladimir Vasilyev on 31.07.2017.
//

import UIKit

class Instrument: NSObject {
    let symbol: SymbolPair
    
    var lastTick: Tick?
    var position: Int?
    
    var enabled: Bool = true
    
    init(with symbol: SymbolPair) {
        self.symbol = symbol
        super.init()
    }
    
    public static func ==(lhs: Instrument, rhs: Instrument) -> Bool {
        return lhs.symbol.rawValue == rhs.symbol.rawValue
    }
    
}
